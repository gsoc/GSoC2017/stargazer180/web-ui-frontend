function vueInit()
{
    /*Vue.component('app', {
        template: '#app-template',
        data: function () {
            return {
                msg: 'Welcome to Vue.js World!'
            }
        }
    });*/

    app = new Vue({
        el: '#app',
        data: {
            playlist: 
            [
                { title: 'vid 1', src: 'videos/01.mp4'  },
                { title: 'vid 2', src: 'videos/02.mp4' }
            ]
        },
        methods: {
            play: function (msg) {
                setVideo(msg);
                player[0].play();
            }
        }
    });
}