function plyrInit()
{
    player = plyr.setup({debug:false,showPosterOnEnd:true});
    setVideo('videos/01.mp4');
    
}

function setVideo(videoSrc)
{
    player[0].source({
        type: 'video',
        title: 'Example title',
        sources: [{
        src: videoSrc,
        type: 'video/mp4'
        }],
        poster: "assets/vlc.png"
    });
}